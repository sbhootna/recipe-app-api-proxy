server{
    listen ${LISTEN_PORT};

    location /static {
        alias /vol/static;
    }   

    location / { 
        uswgi_pass               ${APP_HOST}:${APP_PORT};
        include                  /etc/nginx/uwsgi_param;
        client_max_body_siz      10M;
    }
}